Self Learning 
=============

> To follow the path, look to the master,
> follow the master, walk with the master,
> see through the master, become the master.
>   -- Zen Proverb



This tree contains source code I have written (also in some cases source code
downloaded from text book website) whilst learning the craft of
programming. Categorized by text book name (or abbreviation thereof). I have
attempted to attribute all source files that were typed directly from texts to
their respective authors in the first line of the code. Attribution is generally
given in the source files only (not the header files). However, many files
contain lines of code, or whole functions, taken directly from a text. This has
been done on the path of knowledge and, I believe, is within the spirit with
which the texts were written.

I owe a debt of gratitude to the authors of these texts for having the
opportunity to learn from them. Many thanks.


Directory Listing 
-----------------
__adt__:
Abstract Data Types

__cpl__:  
The C Programming Language - Brian W. Kernighan, Denis M. Ritchie, Second edition.

__blp__:
Beginning Linux Programming - Neil Matthew and Richard Stones.

__lpi__:
The Linux Programming Interface - Michael Kerrisk

__perl/learning-perl__:  
Learning Perl - Randal L. Schwartz, brian d foy and Tom Phoenix, Sixth edition.

__perl/intermediate-perl__:  
Intermediate Perl - Randal L. Schwartz, brian d foy and Tom Phoenix, Second edition.

__pthw__:  
Python the Hard Way - Zed A. Shaw.

__scheme/little-schemer__:  
The Little Schemer - Daniel P. Friedman and Matthais Felleisen (Fourth Edition).

__scheme/seasoned-schemer__:  
The Seasoned Schemer - Daniel P. Friedman and Matthais Felleisen (Fourth
Edition).

__scheme/scheme-programming-language__:
The Scheme Programming Language - R. Kent Dybvig (Fourth Edition).

__scheme/sicp__:
Structure and Interpretation of Computer Programs - Harold Abelson and Gerald
Jay Sussman with Julie Sussman 

__tgpl/__:
The Go Programming Language - Alan A. A. Donovan, Brian W. Kernighan

__UNIX/apue__:  
Advanced Programming in the UNIX Environment - Stevens and Rago, Third Edition.

__UNIX/usp__:  
UNIX SYSTEMS Programming - Kay A. Robbins, Steven Robbins.

__UNIX/unp__:
UNIX Network Programming - Stevens, Fenner, Rudoff, Volume 1, Third Edition.
